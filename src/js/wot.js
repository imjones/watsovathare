var camera;
var video;
var cameraContext;
var rotateX, rotateY, rotateZ;
const locations = [ 'Great Western Academy', 'Harvester North Swindon', 'Toby Carvery North Swindon', 'Blunsdon Arms Swindon', 'Asda Orbital Swindon', 'Astrodome Houston', 'Taj Mahal India Landmark', 'Mount Rushmore', ];
var currentLocation;
var currentBearing = 0;
var markers = [];

function invertColors(imageData) {
  for(var i=0; i<imageData.length; i+=4) {
    imageData[i] = imageData[i] ^ 255;
    imageData[i+1] = imageData[i+1] ^ 255;
    imageData[i+2] = imageData[i+2] ^ 255;
  }
}

function reverseImage(imageData) {
  imageData.reverse();
}

function toRadians(val) {
  return val * (Math.PI/180.0);
}

function toDegrees(val) {
  return val * (180.0/Math.PI);
}

function getBearing(location1, location2) {
  const dLon = (location2.lng - location1.lng);
  const y = Math.cos(toRadians(location2.lat)) * Math.sin(toRadians(dLon));
  const x = Math.cos(toRadians(location1.lat)) * Math.sin(toRadians(location2.lat)) 
            - Math.sin(toRadians(location1.lat)) * Math.cos(toRadians(location2.lat)) * Math.cos(toRadians(dLon));
  const brng = Math.atan2(y, x) * (180.0/Math.PI);
  return (brng + 360) % 360;
}

function getDistance(location1, location2) {
    const earthRadius = 3958.75;
    const dLat = toRadians(location2.lat-location1.lat);
    const dLng = toRadians(location2.lng-location1.lng);
    const sindLat = Math.sin(dLat / 2);
    const sindLng = Math.sin(dLng / 2);
    const a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
            * Math.cos(toRadians(location1.lat)) * Math.cos(toRadians(location2.lat));
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    const dist = earthRadius * c;
    return dist;
}

class Location {
  name = '';
  lat = 0;
  lng = 0;
  bearing = 0;
  distance = 0;
  constructor(name, lat, lng) {
    this.name = name;
    this.lat = lat;
    this.lng = lng;
    if(currentLocation && currentLocation.lat!=0 && currentLocation.lng!=0) {
      this.distance = getDistance(currentLocation, this);
      this.bearing = getBearing(currentLocation, this);
      console.log(name + '(' + this.lat + ', ' + this.lng + ') is on a bearing of ' + this.bearing + ' degrees at a distance of ' + this.distance + ' from (' + currentLocation.lat + ', ' + currentLocation.lng + ')');
    }
  }
}

function createMarker(name, lat, lng)
{
  markers.push(new Location(name, lat, lng));
}

function populateLocations() {
  var service = new google.maps.places.PlacesService(document.createElement('div'));
  for(let ll=0; ll<locations.length; ll++) {
    let request = {
      query: locations[ll],
      fields: ['name', 'geometry'],
    };
    console.log('Finding "' + request.query + '"....');
    service.findPlaceFromQuery(request, processResult);  
  }    
}

function geoFindMe() {
  if(!navigator.geolocation) {
    console.log('Meh.  Cant get location.');
  } else {
    return new Promise( (resolve, reject) => navigator.geolocation.getCurrentPosition(resolve, reject) );
  }
}

function drawVideo() {
  cameraContext.drawImage(video, 0, 0, camera.width, camera.height);
  cameraContext.fillStyle = textColor;
  cameraContext.font = '48px Arial';
  cameraContext.fillText(currentBearing, 100, 100);
  window.requestAnimationFrame(drawVideo);
}

function resizeVideo() {
  let canvasWidth = window.getComputedStyle(camera, null).getPropertyValue('width');
  let canvasHeight = window.getComputedStyle(camera, null).getPropertyValue('height');
  if(screen.orientation.angle===90) {
    canvasWidth = window.getComputedStyle(camera, null).getPropertyValue('height');
    canvasHeight = window.getComputedStyle(camera, null).getPropertyValue('width');    
  }
  console.log("the orientation of the device is now " + screen.orientation.angle + ' Width: ' + canvasWidth + ' Height: ' + canvasHeight);
  video.setAttribute('width', canvasWidth);
  video.setAttribute('height', canvasHeight);
  camera.setAttribute('width', canvasWidth);
  camera.setAttribute('height', canvasHeight);
}

var textColor = 'White';
function handleOrientation(e) {
  let absolute = e.absolute;
  let alpha    = e.alpha;
  let beta     = e.beta;
  let gamma    = e.gamma;

  console.log('Orientation: ' + absolute + ', ' + alpha + ', ' + beta + ', ' + gamma);
  currentBearing = alpha;
}

function handleCompassNeedsCalibration(e) {
  textColor = 'Red';
}

function processResult(results, status) {
  if (status === google.maps.places.PlacesServiceStatus.OK) {
    for (var i = 0; i < results.length; i++) {
      createMarker(results[i].name, results[i].geometry.location.lat(), results[i].geometry.location.lng());
    }
  }
}

function init() {
  camera = document.getElementById('camera');
  cameraContext = camera.getContext('2d');
  currentLocation = new Location('Here', 0, 0);

  // Grab elements, create settings, etc.
  video = document.getElementById('video');
  resizeVideo();

  window.onorientationchange = function() {
      resizeVideo();
  }

  window.addEventListener('deviceorientationabsolute', handleOrientation, true);

  // Get access to the camera!
  if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
    //var cameraDevice = navigator.mediaDevices.getUserMedia({ video: true });
    navigator.mediaDevices.getUserMedia({ video: { facingMode: 'environment' } })
      .then(function(stream) {
        video.srcObject = stream;
        video.play();
      }).catch(function(err) {
          alert(err);
      });
  }

  // Just a quick test to see wtf is going on
  // console.log(getBearing(new Location('Kansas', 39.099912, -94.581213), new Location('St Louis', 38.627089, -90.200203)));

  geoFindMe().then((position) => {
    currentLocation.lat  = position.coords.latitude;
    currentLocation.lng = position.coords.longitude;
    populateLocations();
  });

  window.requestAnimationFrame(drawVideo);
}




